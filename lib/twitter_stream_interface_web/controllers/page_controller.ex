defmodule TwitterStreamInterfaceWeb.PageController do
  use TwitterStreamInterfaceWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
