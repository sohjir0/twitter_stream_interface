defmodule TwitterStreamInterfaceWeb.TweetsChannel do
  use TwitterStreamInterfaceWeb, :channel

  def join("tweets:" <> _topic, _params, socket) do
    {:ok, socket}
  end

  def handle_in("search", _params, socket) do
    "tweets:" <> text = socket.topic
    case TwitterExtract.Stream.Supervisor.search_text(text) do
      {:ok, _pid} ->
        {:reply, :ok, socket}
      {:error, reason} ->
        {:reply, {:error, %{reason: inspect(reason)}}, socket}
    end
  end

end
