defmodule TwitterStreamInterface.Subscriber do
  use GenServer

  alias TwitterExtract.Notifier;

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_args) do
    Notifier.subscribe(self())
    {:ok, []}
  end

  def handle_cast({:incoming, topic, tweet}, pids) do
    TwitterStreamInterfaceWeb.Endpoint.broadcast! "tweets:" <> topic, "incoming", %{topic: topic, tweet: tweet}
    {:noreply, pids}
  end

end
