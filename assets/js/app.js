// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

import socket from "./socket"
import { new_channel, join, search } from "./socket"

document.getElementById("track_button").addEventListener("click", track_text);

export function track_text() {
  let text = document.getElementById("search_text").value
  console.log(`tracking ${text}`);

  var game_channel = new_channel(text);
  join(game_channel);
  search(game_channel);

  if (document.getElementById(`${text}_tab`) === null) {
    let results = document.getElementById("list");

    let tabLi = document.createElement("li");
    tabLi.setAttribute("class", "nav-item");

    let tabData = document.createElement("a");
    tabData.setAttribute("class", "nav-link");
    tabData.setAttribute("role", "tab");
    tabData.setAttribute("id", `${text}_tab`);
    tabData.setAttribute("data-toggle", "tab");
    tabData.setAttribute("href", `#${text}`);
    tabData.setAttribute("aria-control", "home");
    tabData.setAttribute("aria-selected", "true");
    tabData.innerHTML = text;
    tabLi.append(tabData);
    results.append(tabLi);

    let contentData = document.createElement("div")
    contentData.setAttribute("class", "tab-pane");
    contentData.setAttribute("id", `${text}`);
    contentData.setAttribute("role", "tabpanel");
    contentData.setAttribute("aria-labelledby", `${text}-tab`);

    let content = document.getElementById("content");
    content.append(contentData);
  }

  game_channel.on("incoming", response => {
    let divTweet = document.createElement("div");

    let text = document.createElement("p");
    text.innerHTML = `${response.tweet.user_name} - ${response.tweet.text} - ${response.tweet.created_at}`;
    divTweet.append(text);

    let tweets = document.getElementById(`${response.topic}`);
    tweets.insertBefore(divTweet, tweets.childNodes[0]);
    console.log("incoming tweet : ", response)
  });

}

